//
//  Requestable.swift
//  Demo
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

protocol ResponseHandler {
    func handleSuccessWithObject<T>(response: SuccessResponse<T>)
    func handleFailure(error: Error?)
    func handleNetworkError(error: Error)
}

protocol Requestable: ResponseHandler {
    typealias JSONDictionary = [String: Any]
    var method: HTTPMethod { get }
    var urlPath: String { get }
    var pathParameters: [String: String]? { get }
    var queryParameters: [String: String]? { get }
    var requestBody: Codable? { get }
    var costumHeaders: [String: String]? { get }
    
    func executeRequest<T: Codable>(withResponseType: T.Type)
}

extension Requestable {
    
    var costumHeaders: [String: String]? { return nil }
    
    func executeRequest<T: Codable>(withResponseType: T.Type) {
        guard let urlRequest = getURLrequest() else {
            return
        }
        
        let session: URLSession = URLSession.shared
        session.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error as NSError? {
                DispatchQueue.main.async {
                    if error.domain == NSURLErrorDomain {
                        self.handleNetworkError(error: error)
                    } else {
                        self.handleFailure(error: error)
                    }
                }
            } else { // error is nil
                if let response = response,
                    let httpResponse = response as? HTTPURLResponse,
                    let data = data {
                    
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    do {
                        if 200...299 ~= httpResponse.statusCode {
                            let decodedData = try decoder.decode(withResponseType, from: data)
                            let responseResult = SuccessResponse(data: decodedData, response: response)
                            DispatchQueue.main.async {
                                self.handleSuccessWithObject(response: responseResult)
                            }
                        } else {
                            let errorBody = String(data: data, encoding: .utf8) ?? "empty body"
                            fatalError("Request failed, HTTP status code: \(httpResponse.statusCode) \nerror body: \(errorBody)")
                        }
                    } catch (let error) {
                        DispatchQueue.main.async {
                            self.handleFailure(error: error)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.handleFailure(error: nil)
                    }
                }
            }
        }.resume()
    }
    
    private func getURLrequest() -> URLRequest? {

        var baseURL = URL(string: Constants.baseURL)!
        var urlPath = self.urlPath
        if let pathParameters = self.pathParameters {
            pathParameters.forEach {
                urlPath = urlPath.replacingOccurrences(of: "{\($0.key)}", with: $0.value)
            }
        }
        baseURL.appendPathComponent(urlPath)
        
        guard var urlComponents = URLComponents.init(string: (baseURL.absoluteString)) else {
            return nil
        }
        
        if let queryParameters = queryParameters {
            urlComponents.queryItems = queryParameters.map {
                URLQueryItem(name: $0.key, value: String(describing: $0.value))
            }
        }
        var urlRequest = URLRequest(url: urlComponents.url!)
        
        if method != .GET {
            if let requestBody = requestBody {
                let encoder = JSONEncoder()
                encoder.dateEncodingStrategy = .iso8601
                guard let encodedJSON = try? requestBody.encoded() else {
                    fatalError("❌ Failed to encode requestBody")
                }
                urlRequest.httpBody = encodedJSON
            } else {
                fatalError("❌ Error: requestBody is nil")
            }
        }
        urlRequest.httpMethod = method.rawValue
        
        if let customHeaders = self.costumHeaders, !customHeaders.isEmpty {
            for (key, value) in customHeaders {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        return urlRequest
    }
}

extension Encodable {
    func encoded() throws -> Data {
        return try JSONEncoder().encode(self)
    }
}
