//
//  GitHubRequest.swift
//  Demo
//
//  Created by Weixiong Cen on 25/01/21.
//

import Foundation

protocol GitHubRequestDelegate {
    func handleGitHubRequestSuccessWithObject<T>(response: SuccessResponse<T>)
    func handleGitHubRequestFailure()
}

class GitHubRequest: NSObject, Requestable {
    
    var method: HTTPMethod = .GET
    var urlPath: String = ""
    
    let delegate: GitHubRequestDelegate
    
    var pathParameters: [String : String]? = nil
    var queryParameters: [String : String]? = nil
    var requestBody: Codable? = nil
    
    init(delegate: GitHubRequestDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    func handleSuccessWithObject<T>(response: SuccessResponse<T>) {
        delegate.handleGitHubRequestSuccessWithObject(response: response)
    }
    
    func handleFailure(error: Error?) {
        delegate.handleGitHubRequestFailure()
    }
    
    func handleNetworkError(error: Error) {
        delegate.handleGitHubRequestFailure()
    }
}
