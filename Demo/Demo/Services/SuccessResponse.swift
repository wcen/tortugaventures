//
//  SuccessResponse.swift
//  Demo
//
//  Created by Weixiong Cen on 24/01/21.
//

import Foundation

public struct SuccessResponse<T> {
    public var data: T
    public var response: URLResponse
}

