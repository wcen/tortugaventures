//
//  HTTPMethod.swift
//  Demo
//
//  Created by Weixiong Cen on 24/01/21.
//

import Foundation

public enum HTTPMethod: String {
    case GET
    case POST
}
