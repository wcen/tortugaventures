//
//  GitHubResponse.swift
//  Demo
//
//  Created by Weixiong Cen on 25/01/21.
//

import Foundation

struct GitHubResponse: Codable {
    var current_user_url: String?
    var current_user_authorizations_html_url: String?
    var authorizations_url: String?
}
