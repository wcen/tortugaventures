//
//  Constants.swift
//  Demo
//
//  Created by Weixiong Cen on 24/01/21.
//

import Foundation

struct Constants {
    static let baseURL = "https://api.github.com/"
    static let delay = 5.0
    static let historyCellIdentifier = "cell"
    static let cacheResponseFileName = "gitHubResponse.json"
}

