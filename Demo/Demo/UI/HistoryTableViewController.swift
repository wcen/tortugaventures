//
//  HistoryTableViewController.swift
//  Demo
//
//  Created by Weixiong Cen on 25/01/21.
//

import Foundation
import UIKit

class HistoryTableViewController: UITableViewController {
    
    var history: [Date] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.historyCellIdentifier,
                                                 for: indexPath)
        cell.textLabel?.text = "\(history[indexPath.row])"
        if indexPath.row == 0 {
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: cell.textLabel!.font.pointSize)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
}
