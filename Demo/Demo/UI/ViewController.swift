//
//  ViewController.swift
//  Demo
//
//  Created by Weixiong Cen on 24/01/21.
//

import UIKit

class ViewController: UIViewController, GitHubRequestDelegate {
    @IBOutlet weak var currentUserUrlLabel: UILabel!
    @IBOutlet weak var currentUserAuthorizationsHtmlLabel: UILabel!
    @IBOutlet weak var authorizationsUrlLabel: UILabel!
    
    weak var historyTableViewController: HistoryTableViewController?
    
    private var timer: Timer?
    
    private var history: [Date] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lastResponse: GitHubResponse? = FileHelper.read(from: Constants.cacheResponseFileName) ?? nil
        
        currentUserUrlLabel.text = lastResponse?.current_user_url
        currentUserAuthorizationsHtmlLabel.text = lastResponse?.current_user_authorizations_html_url
        authorizationsUrlLabel.text = lastResponse?.authorizations_url
        
        timer = Timer.scheduledTimer(timeInterval: Constants.delay, target: self, selector: #selector(loadData), userInfo: nil, repeats: true)
    }
    
    @objc func loadData() {
        let request = GitHubRequest(delegate: self)
        request.executeRequest(withResponseType: GitHubResponse.self)
    }
    
    func handleGitHubRequestSuccessWithObject<T>(response: SuccessResponse<T>) {
        let gitHubResponse = response.data as? GitHubResponse
        currentUserUrlLabel.text = gitHubResponse?.current_user_url
        currentUserAuthorizationsHtmlLabel.text = gitHubResponse?.current_user_authorizations_html_url
        authorizationsUrlLabel.text = gitHubResponse?.authorizations_url
        
        history.insert(Date(), at: 0)
        historyTableViewController?.history = history
        FileHelper.write(gitHubResponse, to: Constants.cacheResponseFileName)
    }
    
    func handleGitHubRequestFailure() {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let historyTableViewController = segue.destination as? HistoryTableViewController {
            self.historyTableViewController = historyTableViewController
            historyTableViewController.history = history
        }
    }
}

